#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <vector>
#include <algorithm>
#include <boost/bind.hpp>

class SomeClass;

SomeClass* create()
{
	return (SomeClass*)0;
}

void simple_test()
{
//    SomeClass* p = create();

//    delete p; // w najlepszym razie warning
}

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);
}

int main()
{
    int x;
    auto rf = boost::ref(x);

	real_test();

    vector<ToBeDeleted*> vec;

    vec.push_back(new ToBeDeleted());
    vec.push_back(new ToBeDeleted());
    vec.push_back(new ToBeDeleted());

//    for(vector<ToBeDeleted*>::iterator it = vec.begin(); it != vec.end(); ++it)
//        delete *it;

    std::for_each(vec.begin(), vec.end(), &boost::checked_delete<ToBeDeleted>);

    vec.clear();
}
