#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>

using namespace std;

int foo(int arg)
{
    cout << "foo(" << arg << ')' << endl;

    return arg;
}

//template <typename T>
//void foo(T arg, typename T::nested_type* ptr = 0)
//{
//    cout << "foo<T>(" << arg << ") T with nested_type"
//         << endl;
//}


// 1-szy sposob
//template <typename T>
//void foo(T arg, typename boost::disable_if<boost::is_integral<T> >::type* ptr = 0)
//{
//    cout << "foo<T>(" << arg << ')' << endl;
//}

template <typename T>
typename boost::disable_if<boost::is_integral<T>, T>::type foo(T arg)
{
    cout << "foo<T>(" << arg << ')' << endl;

    return arg;
}

struct MyInt
{
    typedef void nested_type;
    int value;
};

ostream& operator<<(ostream& out, const MyInt& mi)
{
    out << mi.value;
    return out;
}

int main()
{
    foo(7);

    int x = 10;

    foo(x);

    short sx = 45;

    foo(sx);

    unsigned int ui = 8;

    foo(ui);

    MyInt mi { 20 };
    foo(mi);
}


