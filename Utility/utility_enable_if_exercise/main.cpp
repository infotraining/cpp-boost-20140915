#include <iostream>
#include <iterator>
#include <list>
#include <boost/type_traits/is_pod.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_float.hpp>
#include <boost/assign.hpp>
#include <cstring>

using namespace std;

// 1 - napisz generyczny algorytm mcopy kopiujący zakres elementow typu T [first, last)
//     do kontenera rozpoczynającego się od dest
// TODO

template <typename InIt, typename OutIt>
void mcopy(InIt start, InIt end, OutIt dest)
{
    cout << "Generic copy..." << endl;

    for(InIt it = start; it != end; ++it)
        *(dest++) = *it;
}

// 2 - napisz zoptymalizowaną wersję mcopy wykorzystującą memcpy dla tablic T[] gdzie typ T jest typem POD
template <typename T>
typename boost::enable_if<boost::is_pod<T> >::type mcopy(T* start, T* end, T* dest)
{
    cout << "Optimized copy..." << endl;

    memcpy(dest, start, (end - start) * sizeof(T));
}

template <typename T, typename Enable = void>
class DataProcessor
{
public:
    void process_data(const list<T>& data)
    {
        cout << "DataProcessor generic version" << endl;
    }
};

template <typename T>
class DataProcessor<T, typename boost::enable_if<boost::is_float<T> >::type>
{
public:
    void process_data(const list<T>& data)
    {
        cout << "DataProcessor float/double version" << endl;
    }
};

int main()
{
    string words[] = { "one", "two", "three", "four" };

    list<string> list_of_words(4);

    mcopy(words, words + 4, list_of_words.begin()); // działa wersja generyczna

    mcopy(list_of_words.begin(), list_of_words.end(), ostream_iterator<string>(cout, " "));
    cout << "\n";

    int numbers[] = { 1, 2, 3, 4, 5 };
    int target[5];

    mcopy(numbers, numbers + 5, target); // działa wersja zoptymalizowana

    mcopy(target, target + 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    using namespace boost::assign;

    list<int> data;

    data += 1, 2, 3, 4, 5, 6, 7, 9;

    DataProcessor<int> dp_int;

    dp_int.process_data(data);

    DataProcessor<float> dp_float;

    list<float> floats = list_of(1.3)(8.9)(9.3);

    dp_float.process_data(floats);
}

