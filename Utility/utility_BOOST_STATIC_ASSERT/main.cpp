#include <iostream>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <memory>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
	BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
	BOOST_STATIC_ASSERT(sizeof(int)==4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
	BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value));
}

template <typename T>
struct is_auto_ptr : public boost::false_type
{
};

template <typename T>
struct is_auto_ptr<std::auto_ptr<T>> : public boost::true_type
{
};

template <typename T>
void use_pointer(T ptr)
{
    BOOST_STATIC_ASSERT(!is_auto_ptr<T>::value && "Nie mozna przekazac auto_ptr do funkcji");
    //static_assert(!is_auto_ptr<T>::value, "Nie mozna przekazac auto_ptr do funkcji");

    std::cout << "use pointer: " << *ptr << std::endl;
}


int main()
{
    int x = 10;
    use_pointer(&x);

    boost::shared_ptr<std::string> sp = boost::make_shared<std::string>("Text");
    use_pointer(sp);

    std::auto_ptr<std::string> ap(new std::string("AutoPtr"));
    use_pointer(ap);

    std::cout << *ap << std::endl;

    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[50];

	accepts_arrays_with_size_between_1_and_100(arr);

    Derived arg;
	works_with_base_and_derived(arg);
}
