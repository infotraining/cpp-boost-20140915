#include <iostream>
#include <memory>
#include <cassert>
#include <vector>
#include <boost/scoped_array.hpp>
#include <boost/scoped_ptr.hpp>

using namespace std;

class Widget
{
    string name_;
public:
    Widget(const string& name = "Empty") : name_(name)
    {
        cout << "Widget(" << name_ << ")" << endl;
    }

    ~Widget()
    {
        cout << "~Widget(" << name_ << ")" << endl;
    }

    string name() const
    {
        return name_;
    }

    void draw() const
    {
        cout << "Drawing widget " << name_ << endl;
    }
};

auto_ptr<Widget> create_ap(const string& name)
{
    return auto_ptr<Widget>(new Widget(name));
}

void sink(auto_ptr<Widget> ptr)
{
    cout << "sink: " << ptr->name() << endl;
}

unique_ptr<Widget> create_up(const string& name)
{
    return unique_ptr<Widget>(new Widget(name));
}

void sink(unique_ptr<Widget> uptr)
{
    cout << "sink uptr: " << uptr->name() << endl;
}

int main()
{
    {
        auto_ptr<Widget> aptr1(new Widget("A"));

        aptr1->draw();

        auto_ptr<Widget> aptr2 = aptr1;

        assert(aptr1.get() == 0);

        aptr2->draw();
    }

    {
        auto_ptr<Widget> aptr3 = create_ap("B");

        sink(aptr3);

        cout << "Before end of scope" << endl;
    }

    cout << "\n\nunique_ptrs:\n";

    {
        unique_ptr<Widget> uptr1(new Widget("UA"));

        uptr1->draw();

        unique_ptr<Widget> uptr2 = move(uptr1);

        // unique_ptr<T>(unique_ptr<T>&&) - konstruktor przenoszacy
    }

    {
        unique_ptr<Widget> uptr3 = make_unique<Widget>("UB");

        uptr3->draw();

        sink(move(uptr3));

        sink(create_up("UC"));

        vector<unique_ptr<Widget>> vec;

        unique_ptr<Widget> uptr4(new Widget("UD"));

        vec.push_back(move(uptr4));
        vec.push_back(create_up("UF"));
        vec.push_back(unique_ptr<Widget>(new Widget("UG")));
        vec.emplace_back(new Widget("UH"));

        // vec.push_back(const T&);
        // vec.push_back(T&&);
    }

    cout << "\n\nArrays:" << endl;

    {
        boost::scoped_array<Widget> arr1(new Widget[10]);

        arr1[0].draw();
    }

    cout << "\n\nArrays by unique_ptr:" << endl;

    {
        unique_ptr<Widget[]> arr1(new Widget[10]);

        arr1[0].draw();
    }
}


