#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <memory>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}

class FileGuard : boost::noncopyable
{
    FILE* file_;

public:
    FileGuard(FILE* file) : file_(file)
    {
        if (!file_)
            throw std::runtime_error("Blad otwarcia pliku!!!");
    }

    ~FileGuard()
    {
        fclose(file_);
    }

    FILE* get() const
    {
        return file_;
    }
};

// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file(fopen(file_name, "w"));

    //FileGuard copy_of_file = file; // error - kopiowanie jest zabronione

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_to_file_with_sp(const char* file_name)
{
    FILE* raw_file = fopen(file_name, "w");

    if (!raw_file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    boost::shared_ptr<FILE> file(raw_file, &fclose);

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

struct FileCloser
{
    void operator()(FILE* f)
    {
        fclose(f);
    }
};

void save_to_file_with_up(const char* file_name)
{
    //std::unique_ptr<FILE, FileCloser> file(fopen(file_name, "w"));
    //std::unique_ptr<FILE, void(*)(FILE*)> file(fopen(file_name, "w"), &fclose);

    auto fcloser = [](FILE* file) { fclose(file); };

    std::unique_ptr<FILE, decltype(fcloser)> file(fopen(file_name, "w"), fcloser);

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    //save_to_file_with_sp("text.txt");
    save_to_file_with_up("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

    std::cout << "Press a key..." << std::endl;
    std::string temp;
    std::getline(std::cin, temp);
}
