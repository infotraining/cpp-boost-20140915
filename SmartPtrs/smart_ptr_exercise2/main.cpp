#include <iostream>
#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <memory>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjątki #1
********************************************************************/

class Device
{
private:
	size_t devno_;
public:
	Device(int devno) : devno_(devno)
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Konstruktor Device #" << devno << endl;
	}

	~Device()
	{
		cout << "Destruktor Device #" << devno_ << endl;
	}

};

class Broker 
{
private:
    unique_ptr<Device> dev1_;
    unique_ptr<Device> dev2_;
public:
    Broker(int devno1, int devno2) : dev1_(new Device(devno1)), dev2_(new Device(devno2))
	{
	}

    Broker(const Broker& source) : dev1_(new Device(*(source.dev1_))), dev2_(new Device(*(source.dev2_)))
    {}

    Broker(Broker&& source) : dev1_(move(source.dev1_)), dev2_(move(source.dev2_))
    {}
};

int main()
{
	try
	{
        Broker b(1, 3);
        Broker copy_of_b = b;
        Broker moved_b = move(b);
	}
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}
}
