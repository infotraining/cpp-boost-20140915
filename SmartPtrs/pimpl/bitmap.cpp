#include "bitmap.hpp"

using namespace std;

struct Bitmap::Impl
{
    vector<char> buffer_;

    Impl(size_t size) : buffer_(size)
    {}
};

Bitmap::Bitmap() : impl_(new Impl(1024))
{
    std::fill(impl_->buffer_.begin(), impl_->buffer_.end(), 65);
}

Bitmap::~Bitmap() = default;

void Bitmap::draw() const
{
    // ...

    for(size_t i = 0; i < impl_->buffer_.size(); ++i)
        std::cout << impl_->buffer_[i];
    std::cout << std::endl;
}
