#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <algorithm>
#include <vector>
#include <boost/scoped_ptr.hpp>

class Bitmap
{
    class Impl;

    boost::scoped_ptr<Impl> impl_;

public:
    Bitmap();
    ~Bitmap();
    void draw() const;
};


#endif // BITMAP_HPP

