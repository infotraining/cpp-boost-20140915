#include <iostream>
#include <boost/function.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/bind.hpp>

using namespace std;

void func2(int x, int y)
{
    cout << "func2(" << x << ", " << y << ")" << endl;
}

class Func2
{
public:
    void operator()(int x, int y)
    {
        cout << "Func2::operator()(" << x << ", " << y << ")" <<  endl;
    }
};


string func3(const string& text, int value, int& counter)
{
    ++counter;

    return text + " - " + boost::lexical_cast<string>(value);
}

class Add
{
public:
    typedef int result_type;

    int operator()(int a, int b)
    {
        return a + b;
    }
};

class Service
{
public:
    typedef boost::function<void (string)> OnRunCallback;
    typedef boost::function<void ()> OnCloseCallback;

    void run(const string& arg)
    {
        if (on_run_callback_)
            on_run_callback_(arg);

        cout << "Service is running..." << endl;
    }

    void close()
    {
        if (on_close_callback_)
            on_close_callback_();
        cout << "Service is closed..." << endl;
    }

    void set_on_run_callback(OnRunCallback callback)
    {
        on_run_callback_ = callback;
    }

    void set_on_close_callback(OnCloseCallback callback)
    {
        on_close_callback_ = callback;
    }

private:
        boost::function<void (string)> on_run_callback_;
        boost::function<void ()> on_close_callback_;
};

namespace MyLogger
{
    void log(const string& msg)
    {
        cout << "Log: " << msg << endl;
    }
}

class Db
{
public:
    void save_to_db(const string& table_name, const string& value)
    {
        cout << "Saving to " << table_name << " entry " << value << endl;
    }
};

int main()
{
    Db database;

    boost::function<void (int, int)> f1 = &func2;

    f1(6, 7);

    Func2 f;
    f1 = f;

    f1(8, 4);

    f1 = [](int a, int b) { cout << "lambda: " << a << ", " << b << endl; };

    f1(9, 12);

    Service srv;

    //srv.set_on_run_callback(&MyLogger::log);

    srv.set_on_run_callback(boost::bind(&Db::save_to_db, &database, "Logs", _1));
    srv.run("Service test");

    srv.set_on_close_callback(
        boost::bind(&Db::save_to_db, boost::ref(database),"Logs", "Service closed"));

    srv.set_on_close_callback([&] { database.save_to_db("Logs", "Service closed"); });

    srv.close();
}

