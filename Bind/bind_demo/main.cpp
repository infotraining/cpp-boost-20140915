#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <map>
#include <iterator>

using namespace std;

void func2(int x, double y)
{
    cout << "func2(" << x << ", " << y << ")" << endl;
}

string func3(const string& text, int value, int& counter)
{
    ++counter;

    return text + " - " + boost::lexical_cast<string>(value);
}

class Add
{
public:
    typedef int result_type;

    int operator()(int a, int b)
    {
        return a + b;
    }
};

class BoundFunc
{
public:
    void operator()(int arg)
    {
        func2(10, arg);
    }
};

class Person
{
    int id_;
    string name_;
    int age_;
public:
    Person(int id, string name, int age) : id_(id), name_(name), age_(age)
    {}

    int age() const
    {
        return age_;
    }

    bool is_retired() const
    {
        return age_ > 67;
    }

    void print(string prefix) const
    {
        cout << prefix << " " << id_ << " - " << name_ << " - " << age_ << endl;
    }
};


int main()
{
    func2(8, 2);

    int counter = 0;

    cout << func3("Text", 13, counter) << endl;

    auto f1 = boost::bind(&func2, 10, _1);

    f1(17); // func2(10, 17)

    auto f2 = boost::bind(&func2, _1, -1);

    f2(10); // func2(10, -1)

    auto f3 = boost::bind(&func2, 8, 9);

    f3();

    auto f4 = boost::bind(&func2, _2, _1);

    f4(10, 1); // func2(1, 10)

    string prefix = "Prefix";

    auto f5 = boost::bind(&func3, boost::cref(prefix), 10, boost::ref(counter));

    cout << f5() << endl;
    cout << f5() << endl;

    cout << "counter: " << counter << endl;

    auto f6 = boost::bind(plus<int>(), _1, 10);

    cout << f6(123) << endl;

    vector<Person> people = { Person(1, "Kowalski", 44), Person(2, "Nowak", 77),
                              Person(3, "Nijaki", 68), Person(8, "Anonim", 33) };

    for_each(people.begin(), people.end(),
             boost::bind(&Person::print, _1, "Person"));

    cout << "\n\n";

    vector<boost::shared_ptr<Person>> sp_people =
        { boost::make_shared<Person>(1, "Kowalski", 44),
          boost::make_shared<Person>(2, "Nowak", 77),
          boost::make_shared<Person>(3, "Nijaki", 68),
          boost::make_shared<Person>(8, "Anonim", 33) };

    for_each(sp_people.begin(), sp_people.end(),
             boost::bind(&Person::print, _1, "Person"));

    vector<Person> retired;

    copy_if(people.begin(), people.end(), back_inserter(retired),
            boost::bind(&Person::is_retired, _1));

    vector<Person> employees;

    copy_if(people.begin(), people.end(), back_inserter(employees),
            !boost::bind(&Person::is_retired, _1));

    cout << "\n\nRetired:\n";
    for_each(retired.begin(), retired.end(),
             boost::bind(&Person::print, _1, "Person"));

    typedef map<string, string> Dictionary;

    Dictionary dict = { { "One", "Jeden" }, { "Two", "Dwa" },
                                 { "Three", "Trzy" } , { "Six", "Szesc" } };

    transform(dict.begin(), dict.end(), ostream_iterator<string>(cout, " "),
              boost::bind(&decltype(dict)::value_type::first, _1));

    cout << "\n\n";

//    auto gt40 = find_if(people.begin(), people.end(),
//                        boost::bind(greater<int>(),
//                                        boost::bind(&Person::age, _1),
//                                        40));

    auto gt40 = find_if(people.begin(), people.end(),
                        boost::bind(&Person::age, _1) > 40);

    if (gt40 != people.end())
        gt40->print("gt40: ");
}

