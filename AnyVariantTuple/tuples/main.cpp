#include <iostream>
#include <algorithm>
#include <vector>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/fusion/adapted/boost_tuple.hpp>
#include <boost/fusion/include/boost_tuple.hpp>
#include <boost/fusion/algorithm.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

using namespace std;

boost::tuple<int, int> find_min_max(const vector<int>& vec)
{
    auto it_min = min_element(vec.begin(), vec.end());
    auto it_max = max_element(vec.begin(), vec.end());

    return boost::make_tuple(*it_min, *it_max);
}


template <typename Tuple, typename Func, size_t Index>
struct ForEachHelper
{
    static void for_each(const Tuple& t, Func f)
    {
        ForEachHelper<Tuple, Func, Index-1>::for_each(t, f);
        f(boost::tuples::get<Index>(t));
    }
};

template <typename Tuple, typename Func>
struct ForEachHelper<Tuple, Func, 0>
{
    static void for_each(const Tuple& t, Func f)
    {
        f(boost::tuples::get<0>(t));
    }
};

template <typename Tuple, typename Func>
void tuple_for_each(const Tuple& t, Func f)
{
    ForEachHelper<Tuple, Func, boost::tuples::length<Tuple>::value-1>::for_each(t, f);
}


template <typename Tuple, typename Func>
void simpler_for_each(const Tuple& t, Func f)
{
    f(t.get_head());

    simpler_for_each(t.get_tail(), f);
}

template <typename Func>
void simpler_for_each(const boost::tuples::null_type, Func f)
{
}

struct X
{
    int a;
    int b;
    double c;
};

class Formatter
{
public:
    void operator()(int arg) const
    {
        cout << "<int>" << arg << "</int>" << endl;
    }

    void operator()(double arg) const
    {
        cout << "<double>" << arg << "</double>" << endl;
    }

    void operator()(string arg) const
    {
        cout << "<string>" << arg << "</string>" << endl;
    }
};

BOOST_FUSION_ADAPT_STRUCT(
    X,
    (int, a)
    (int, b)
    (double, c))

int main()
{
    typedef boost::tuple<int, double, string> Tuple3;

    boost::tuple<int, double, string> triple(8, 3.1415, "Krotka...");

    cout << boost::tuples::set_open('[') << boost::tuples::set_close(']')
        << boost::tuples::set_delimiter(',')
        << "triple: " << triple << endl;

    cout << "triple<0>: " << triple.get<0>() << " "
         << "triple<1>: " << boost::get<1>(triple) << " "
         << "triple<2>: " << triple.get<2>() << endl;

    Tuple3 default_triple;

    cout << "default_triple: " << default_triple << endl;

    default_triple = triple;

    cout << "default_triple: " << default_triple << endl;
    cout << "triple: " << triple << endl;

    vector<int> numbers = { 4, 2, 7, 8, -4, 33, 103, 126, 9, 0 };

    boost::tuple<int, int> minmax = find_min_max(numbers);

    cout << "min: " << minmax.get<0>() << "\n";
    cout << "max: " << boost::get<1>(minmax) << "\n";

    int x = 0;
    string str = "str";

    boost::tuple<int&, const string&> ref_tuple(x, str);

    ref_tuple.get<0>() = 10;
    cout << x << endl;

    //ref_tuple.get<1>() = "text";

    int min_val;
    int max_val;

//    boost::tuple<int&, int&> ref_minmax(min_val, max_val);
//    ref_minmax = find_min_max(numbers);

    boost::tie(min_val, max_val) = find_min_max(numbers);

    cout << "min_val: " << min_val << endl;
    cout << "max_val: " << max_val << endl;

    int minimum;

    boost::tie(minimum, boost::tuples::ignore) = find_min_max(numbers);

    cout << "minimum: " << minimum << endl;

    boost::tuple<int&, double, string> t3 = { minimum, 3.14, "String" };

    cout << "t3: " << t3 << endl;

    t3.get<0>() = -9;

    cout << minimum << endl;

    cout << "Size of tuple: " << boost::tuples::length<Tuple3>::value << endl;

    Tuple3 test_tuple = { 1, 7.666, "Test" };

    cout << "test_tuple: " << test_tuple << endl;

    simpler_for_each(test_tuple, Formatter());

    Formatter f;

    f(test_tuple.get<0>());
    f(test_tuple.get<1>());
    f(test_tuple.get<2>());

    X item { 1, 4, 3.14 };

    cout << "\n\n";
    tuple_for_each(boost::tie(item.a, item.b, item.c), Formatter());

    cout << "\n\nFusion:\n";
    boost::fusion::for_each(test_tuple, Formatter());

    cout << "\n\nFusion with struct:\n";

    boost::fusion::for_each(item, Formatter());
}



