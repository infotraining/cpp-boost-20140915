#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>
#include <boost/range/algorithm.hpp>

using namespace std;

struct Modulus : public boost::static_visitor<double>
{
    typedef double result_type;

    template <typename T>
    result_type operator()(const T& arg) const
    {
        return abs(arg);
    }

    result_type operator()(double arg) const
    {
        return fabs(arg);
    }
};


int main()
{
    typedef boost::variant<int, float, complex<double> > VariantNumber;
    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(-7);
    vars.push_back(complex<double>(-1, -1));

    // TODO: korzystając z mechanizmu wizytacji wypisać na ekranie moduły liczb
    Modulus mod;

    boost::transform(vars, ostream_iterator<double>(cout, " "),
                     boost::apply_visitor(mod));

    cout << endl;
}
