#include <iostream>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace std;
using namespace boost::signals2;

class Service
{
public:
    typedef signal<void (string)> OnRunSignal;
    typedef OnRunSignal::slot_type OnRunSlot;

    void run(const string& arg)
    {
        on_run_signal_(arg);

        cout << "Service is running..." << endl;
    }

    void close()
    {
        cout << "Service is closed..." << endl;
    }

    connection register_on_run_callback(const OnRunSlot& callback)
    {
        return on_run_signal_.connect(callback);
    }

private:
    OnRunSignal on_run_signal_;
};

void log_msg(const string& msg)
{
    cout << "Log: " << msg << endl;
}

class Db
{
public:
    void save_to_db(const string& table_name, const string& value)
    {
        cout << "Saving to " << table_name << " entry " << value << endl;
    }
};

class Printer
{
public:
    void print(const string& msg)
    {
        cout << "Printing " << msg << endl;
    }
};

int main()
{
    Db database;
    Service srv;

    srv.register_on_run_callback(&log_msg);
    auto conn_to_db = srv.register_on_run_callback(boost::bind(&Db::save_to_db, &database, "Logs", _1));

    {
        boost::shared_ptr<Printer> prn = boost::make_shared<Printer>();

        srv.register_on_run_callback(
                    Service::OnRunSlot(&Printer::print, prn.get(), _1).track(prn));


        srv.run("Test");

        conn_to_db.disconnect();

        srv.run("Later");
    }

    cout << "After scope..." << endl;

    srv.run("After scope");
}

