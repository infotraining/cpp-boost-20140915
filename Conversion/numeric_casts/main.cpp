#include <iostream>
#include <boost/cast.hpp>

using namespace std;

int main()
{
    int x = 999;

    short sx = boost::numeric_cast<short>(x);

    cout << "x = " << x << "\n";
    cout << "sx = " << sx << "\n";

    x = 1;

    unsigned int ux = boost::numeric_cast<unsigned int>(x);

    cout << "x = " << x << "\n";
    cout << "ux = " << ux << "\n";
}

